import os, sys
import pickle
import math
import numpy as np

from PIL import Image

import keras.backend as K
from keras.models import Sequential, Model
from keras.layers import (
        Input, Conv2D, Conv2DTranspose, UpSampling2D, Cropping2D, Lambda, Flatten)
from keras.losses import binary_crossentropy
from keras.optimizers import Adam

def progress(batch, n_batches):
    length = 50
    done = (batch + 1) * length // n_batches
    togo = length - done
    prog = "[" + done * "=" + togo * "-" + "]"
    sys.stdout.write(prog)
    sys.stdout.write(", batch {}/{}".format(batch+1, n_batches))
    if(batch + 1 < n_batches):
        sys.stdout.write('\r')
    else:
        sys.stdout.write('\n')
    sys.stdout.flush()


def tile(X, rows, cols):
    tiling = np.ones((rows * X.shape[1], cols * X.shape[2], X.shape[3]),
            dtype = X.dtype) * 255
    for i in range(rows):
        for j in range(cols):
            idx = i * cols + j
            if idx < X.shape[0]:
                img = X[idx,...]
                tiling[i*X.shape[1]:(i+1)*X.shape[1],
                        j*X.shape[2]:(j+1)*X.shape[2],
                        :] = img
    return tiling

def plot_translation(orig, results, title):
    X = np.concatenate([orig, results[0], results[1]])
    X = np.clip(X, 0.0, 1.0)
    canvas = tile(X, 3, results[0].shape[0])
    fname = title + ".png"
    Image.fromarray(np.uint8(255*canvas)).save(fname)
    print("Wrote " + fname)


def set_trainability(model, trainable):                                                                                                                      
    """Recursively set trainability of all layers contained in a model."""
    if isinstance(model, (Sequential, Model)):
        for l in model.layers:
            set_trainability(l, trainable)
    model.trainable = trainable


class ImageToImageTranslator(object):
    def __init__(self, img_shape):
        self.img_shape = img_shape

        self.define_graph()


    def define_graph(self):
        self.common_encoder = self.make_common_encoder_model()
        self.latent_space_shape = self.common_encoder.layers[-1].output_shape[1:4] 
        self.common_generator = self.make_common_generator_model()
        self.common_discriminator = self.make_common_discriminator_model()

        self.encoders = []
        self.encoders.append(self.make_encoder_model(self.common_encoder))
        self.encoders.append(self.make_encoder_model(self.common_encoder))

        self.sampling = self.make_sampling_layer()

        self.generators = []
        self.generators.append(self.make_generator_model(self.common_generator))
        self.generators.append(self.make_generator_model(self.common_generator))

        self.discriminators = []
        self.discriminators.append(self.make_discriminator_model(self.common_discriminator))
        self.discriminators.append(self.make_discriminator_model(self.common_discriminator))

        (generative_training, discriminative_training,
                translation1_model, translation2_model) = self.make_complete_model(
                self.encoders, self.sampling, self.generators, self.discriminators)

        self.generative_training = generative_training
        self.discriminative_training = discriminative_training
        self.translation1_model = translation1_model
        self.translation2_model = translation2_model

        generative_optimizer = Adam()
        discriminative_optimizer = Adam()
        identity_loss = lambda y_true, y_pred: K.mean(y_pred, axis = -1)

        for discriminator in self.discriminators:
            set_trainability(discriminator, False)
        self.generative_training.compile(
                loss = identity_loss,
                optimizer = generative_optimizer)

        for discriminator in self.discriminators:
            set_trainability(discriminator, True)
        for encoder in self.encoders:
            set_trainability(encoder, False)
        for generator in self.generators:
            set_trainability(generator, False)
        self.discriminative_training.compile(
                loss = identity_loss,
                optimizer = discriminative_optimizer)


    def make_common_encoder_model(self):
        input_shape = (self.img_shape[0] // 2, self.img_shape[1] // 2, 32)
        kernel_initializer = "he_normal"
        activation = "relu"
        padding = "same"

        model = Sequential()
        model.add(Conv2D(
                            filters = 64,
                            kernel_size = 5,
                            strides = 2,
                            activation = activation,
                            padding = padding,
                            kernel_initializer = kernel_initializer,
                            input_shape = input_shape))
        model.add(Conv2D(
                            filters = 128,
                            kernel_size = 7,
                            strides = 2,
                            activation = activation,
                            padding = "valid",
                            kernel_initializer = kernel_initializer))
        model.add(Conv2D(
                            filters = 256,
                            kernel_size = 1,
                            strides = 2,
                            activation = activation,
                            padding = padding,
                            kernel_initializer = kernel_initializer))
        print("Common Encoder Internal:")
        model.summary()

        mu_layer = Conv2D(
                            filters = 256,
                            kernel_size = 1,
                            strides = 1,
                            activation = None,
                            padding = padding,
                            kernel_initializer = kernel_initializer)
        var_layer = Conv2D(
                            filters = 256,
                            kernel_size = 1,
                            strides = 1,
                            activation = "softplus",
                            padding = padding,
                            kernel_initializer = kernel_initializer)

        encoder_input = Input(shape = input_shape)
        features = model(encoder_input)
        encoder_output = [mu_layer(features), var_layer(features)]
        encoder = Model(encoder_input, encoder_output)
        print("Common Encoder:")
        encoder.summary()

        return encoder


    def make_common_generator_model(self):
        input_shape = self.latent_space_shape
        kernel_initializer = "he_normal"
        activation = "relu"
        padding = "same"

        model = Sequential()
        model.add(UpSampling2D(input_shape = input_shape))
        model.add(Conv2D(
                            filters = 256,
                            kernel_size = 3,
                            strides = 1,
                            activation = activation,
                            padding = padding,
                            kernel_initializer = kernel_initializer))
        model.add(UpSampling2D())
        model.add(Conv2D(
                            filters = 128,
                            kernel_size = 3,
                            strides = 1,
                            activation = activation,
                            padding = padding,
                            kernel_initializer = kernel_initializer))
        model.add(UpSampling2D())
        model.add(Conv2D(
                            filters = 64,
                            kernel_size = 3,
                            strides = 1,
                            activation = activation,
                            padding = padding,
                            kernel_initializer = kernel_initializer))
        model.add(Cropping2D(cropping = ((0,1),(0,1))))
        model.add(UpSampling2D())
        model.add(Conv2D(
                            filters = 64,
                            kernel_size = 3,
                            strides = 1,
                            activation = activation,
                            padding = padding,
                            kernel_initializer = kernel_initializer))
        print("Common Generator:")
        model.summary()

        return model


    def make_common_discriminator_model(self):
        input_shape = (self.img_shape[0] // 2, self.img_shape[1] // 2, 32)
        kernel_initializer = "he_normal"
        activation = "relu"
        padding = "same"

        model = Sequential()
        model.add(Conv2D(
                            filters = 64,
                            kernel_size = 5,
                            strides = 2,
                            activation = activation,
                            padding = padding,
                            kernel_initializer = kernel_initializer,
                            input_shape = input_shape))
        model.add(Conv2D(
                            filters = 128,
                            kernel_size = 7,
                            strides = 2,
                            activation = activation,
                            padding = "valid",
                            kernel_initializer = kernel_initializer))
        model.add(Conv2D(
                            filters = 256,
                            kernel_size = 1,
                            strides = 2,
                            activation = activation,
                            padding = padding,
                            kernel_initializer = kernel_initializer))
        model.add(Conv2D(
                            filters = 1,
                            kernel_size = 1,
                            strides = 1,
                            activation = "sigmoid",
                            padding = padding,
                            kernel_initializer = kernel_initializer))
        model.add(Flatten())
        print("Common Discriminator:")
        model.summary()

        return model


    def make_encoder_model(self, common_model):
        input_shape = self.img_shape
        kernel_initializer = "he_normal"
        activation = "relu"
        padding = "same"

        model = Sequential()
        model.add(Conv2D(
                            filters = 32,
                            kernel_size = 5,
                            strides = 2,
                            activation = activation,
                            padding = padding,
                            kernel_initializer = kernel_initializer,
                            input_shape = input_shape))
        print("Encoder Internal:")
        model.summary()

        encoder_input = Input(shape = input_shape)
        unique_features = model(encoder_input)
        encoder_output = common_model(unique_features)
        encoder = Model(encoder_input, encoder_output)
        print("Encoder:")
        encoder.summary()

        return encoder


    def make_generator_model(self, common_model):
        input_shape = common_model.layers[-1].output_shape[1:4] 
        kernel_initializer = "he_normal"
        activation = "relu"
        padding = "same"

        model = Sequential()
        model.add(UpSampling2D(input_shape = input_shape))
        model.add(Conv2D(
                            filters = 32,
                            kernel_size = 3,
                            strides = 1,
                            activation = activation,
                            padding = padding,
                            kernel_initializer = kernel_initializer))
        model.add(Conv2D(
                            filters = 3,
                            kernel_size = 1,
                            strides = 1,
                            activation = "tanh",
                            padding = padding,
                            kernel_initializer = kernel_initializer))
        print("Generator Internal:")
        model.summary()

        generator_input = Input(shape = self.latent_space_shape)
        common_features = common_model(generator_input)
        generator_output = model(common_features)
        generator = Model(generator_input, generator_output)
        print("Generator:")
        generator.summary()

        return generator


    def make_discriminator_model(self, common_model):
        input_shape = self.img_shape
        kernel_initializer = "he_normal"
        activation = "relu"
        padding = "same"

        model = Sequential()
        model.add(Conv2D(
                            filters = 32,
                            kernel_size = 5,
                            strides = 2,
                            activation = activation,
                            padding = padding,
                            kernel_initializer = kernel_initializer,
                            input_shape = input_shape))
        print("Internal Discriminator:")
        model.summary()

        discriminator_input = Input(shape = input_shape)
        unique_features = model(discriminator_input)
        discriminator_output = common_model(unique_features)
        discriminator = Model(discriminator_input, discriminator_output)
        print("Discriminator:")
        discriminator.summary()

        return discriminator


    def make_sampling_layer(self):
        def sample(args):
            mean, diag_var = args
            shape = K.shape(mean)
            eps = K.random_normal(
                    shape = shape,
                    mean = 0.0, stddev = 1.0)
            return mean + K.sqrt(diag_var) * eps

        return Lambda(sample)


    def make_complete_model(
            self, encoders, sampling, generators, discriminators):
        x1 = Input(shape = self.img_shape)
        x2 = Input(shape = self.img_shape)

        e1x1 = encoders[0](x1)
        e2x2 = encoders[1](x2)

        z1 = sampling(e1x1)
        z2 = sampling(e2x2)

        g11 = generators[0](z1)
        g12 = generators[1](z1)
        g21 = generators[0](z2)
        g22 = generators[1](z2)

        d1_11 = discriminators[0](g11)
        d1_21 = discriminators[0](g21)
        d2_12 = discriminators[1](g12)
        d2_22 = discriminators[1](g22)

        d1_00 = discriminators[0](x1)
        d2_00 = discriminators[1](x2)

        def likelihood(mean, sample):
            # - \log N_{mean, I}(sample)
            mean = K.batch_flatten(mean)
            sample = K.batch_flatten(sample)
            N = K.cast(K.shape(mean)[-1], "float32")
            return (
                    #-0.5 * N * K.log(2.0*math.pi) # not really important for the objective
                    -0.5 * K.sum(K.square(mean - sample), axis = -1))

        def prior(params):
            mean, diagvar = params
            mean = K.batch_flatten(mean)
            diagvar = K.batch_flatten(diagvar)
            return 0.5 * K.sum(K.square(mean) + diagvar - K.log(diagvar) - 1, axis = -1)

        def generative_loss_function(args):
            x1, x2, e1x1_mean, e1x1_var, e2x2_mean, e2x2_var, g11, g22, d1_11, d2_12, d1_21, d2_22 = args
            # we have to unpack the encoder's parameter to pass
            # them as inputs to this layer - repack now
            e1x1 = [e1x1_mean, e1x1_var]
            e2x2 = [e2x2_mean, e2x2_var]

            # since we are performing binary classification with the
            # discriminators it suffices to have them produce only a single
            # scalar which by convention we take to be the probability for
            # the sample coming from the true data distribution
            l_true = K.ones_like(d1_11)
            l_gen = K.zeros_like(d1_11)

            # minimizing the negative crossentropy of the undesired label
            # instead of maximizing the crossentropy for the desired label
            # seems might have worse gradients according to Goodfellow,
            # but we are following the original paper
            lambda2 = 1.0 / math.sqrt(256)
            loss = (
                    # terms to fool the discriminator
                    -0.5 * binary_crossentropy(y_true = l_gen, y_pred = d1_11)
                    -0.5 * binary_crossentropy(y_true = l_gen, y_pred = d2_12)
                    -0.5 * binary_crossentropy(y_true = l_gen, y_pred = d1_21)
                    -0.5 * binary_crossentropy(y_true = l_gen, y_pred = d2_22)
                    # reconstruction error / likelihood
                    - likelihood(g11, x1)
                    - likelihood(g22, x2)
                    # prior on latent space
                    + lambda2 * prior(e1x1)
                    + lambda2 * prior(e2x2))
            return K.expand_dims(loss)

        def discriminative_loss_function(args):
            d1_00, d2_00, d1_11, d2_12, d1_21, d2_22 = args

            # see generative_loss_function
            l_true = K.ones_like(d1_11)
            l_gen = K.zeros_like(d1_11)

            loss = (
                    # terms to recognize samples
                    binary_crossentropy(y_true = l_true, y_pred = d1_00)
                    + binary_crossentropy(y_true = l_true, y_pred = d2_00)
                    # terms to detect generations
                    +0.5 * binary_crossentropy(y_true = l_gen, y_pred = d1_11)
                    +0.5 * binary_crossentropy(y_true = l_gen, y_pred = d2_12)
                    +0.5 * binary_crossentropy(y_true = l_gen, y_pred = d1_21)
                    +0.5 * binary_crossentropy(y_true = l_gen, y_pred = d2_22))
            return K.expand_dims(loss)

        generative_inputs = [x1, x2]
        generative_loss = Lambda(generative_loss_function)(
                [x1, x2, e1x1[0], e1x1[1], e2x2[0], e2x2[1], g11, g22, d1_11, d2_12, d1_21, d2_22])
        generative_training = Model(
                generative_inputs, generative_loss)
        print("Generative Training:")
        generative_training.summary()

        discriminative_inputs = [x1, x2]
        discriminative_loss = Lambda(discriminative_loss_function)(
                [d1_00, d2_00, d1_11, d2_12, d1_21, d2_22])
        discriminative_training = Model(
                discriminative_inputs, discriminative_loss)
        print("Discriminative Training:")
        discriminative_training.summary()

        translation1_model = Model(x1, [g11, g12])
        translation2_model = Model(x2, [g21, g22])

        return (generative_training, discriminative_training,
                translation1_model, translation2_model)


    def pre_epoch(self):
        self.dt = []
        self.gt = []
        n_samples = 16
        if not hasattr(self, "sample_indices"):
            self.sample_indices = np.random.choice(
                    self.X1_test.shape[0], size = n_samples, replace = False)
        X1 = self.X1_test[self.sample_indices,...]
        X2 = self.X2_test[self.sample_indices,...]
        from1 = self.translation1_model.predict_on_batch(X1)
        from2 = self.translation2_model.predict_on_batch(X2)
        plot_translation(X1, from1, "from1_{:04}".format(self.epoch))
        plot_translation(X2, from2, "from2_{:04}".format(self.epoch))



    def post_epoch(self):
        self.epoch += 1
        dt = np.mean(self.dt)
        gt = np.mean(self.gt)
        print("Discriminative\t\tGenerative")
        print("{:14.6}\t\t{:14.6}".format(dt, gt))


    def get_generative_weights(self):
        weights = []
        for model in self.encoders + self.generators:
            for layer in model.layers:
                for w in layer.get_weights():
                    weights.append(w)
        return weights


    def pre_dt(self):
        # see post_dt
        self.prev_gen_weights = self.get_generative_weights()


    def post_dt(self):
        # make sure weights of the generative part did not change during
        # discriminative training
        weights = self.get_generative_weights()
        for w1, w2 in zip(weights, self.prev_gen_weights):
            diff = w1 != w2
            if diff.any():
                print(w1[diff])
                print(w2[diff])
                assert(not diff.any())


    def fit(self, X1, X2, X1_test, X2_test):
        self.X1_test = X1_test
        self.X2_test = X2_test
        self.X1 = X1
        self.X2 = X2
        self.epoch = 0
        batch_size = 128
        n_epochs = 30
        assert(X1.shape == X2.shape)
        n_batches = X1.shape[0] // batch_size
        target_dummy = np.zeros(batch_size)
        for epoch in range(n_epochs):
            self.pre_epoch()
            epoch_indices11 = np.random.permutation(X1.shape[0])
            epoch_indices21 = np.random.permutation(X2.shape[0])
            epoch_indices12 = np.random.permutation(X1.shape[0])
            epoch_indices22 = np.random.permutation(X2.shape[0])
            X1_epoch1 = X1[epoch_indices11,...]
            X2_epoch1 = X2[epoch_indices21,...]
            X1_epoch2 = X1[epoch_indices12,...]
            X2_epoch2 = X2[epoch_indices22,...]
            for batch in range(n_batches):
                X1_batch = X1_epoch1[batch*batch_size:(batch+1)*batch_size,...]
                X2_batch = X2_epoch1[batch*batch_size:(batch+1)*batch_size,...]
                self.pre_dt()
                dt = self.discriminative_training.train_on_batch(
                        [X1_batch, X2_batch], target_dummy)
                self.post_dt()
                X1_batch = X1_epoch2[batch*batch_size:(batch+1)*batch_size,...]
                X2_batch = X2_epoch2[batch*batch_size:(batch+1)*batch_size,...]
                gt = self.generative_training.train_on_batch(
                        [X1_batch, X2_batch], target_dummy)
                bsize = X1_batch.shape[0] + X2_batch.shape[0]
                self.dt.append(dt/bsize)
                self.gt.append(gt/bsize)
                progress(batch, n_batches)
            self.post_epoch()

    def translate12(self, X1):
        pass

    def translate21(self, X2):
        pass




def load_data(path):
    names = ["mnist_domain_1", "mnist_domain_2"]
    data = []
    for name in names:
        fname = os.path.join(path, name + ".p")
        with open(fname, "rb") as f:
            data.append(pickle.load(f))
    return data


def split(X1, X2):
    np.random.seed(42)
    N = X1.shape[0]
    assert(X2.shape[0] == N)
    perm = np.random.permutation(N)
    return X1[:N//2,...], X2[N//2:,...]


if __name__ == "__main__":
    import sys

    if len(sys.argv) != 2:
        print("Useage: {} <path to data>".format(sys.argv[0]))
        exit(1)
    path = sys.argv[1]

    data = load_data(path)
    (X1_train, y1_train), (X1_test, y1_test) = data[0]
    (X2_train, y2_train), (X2_test, y2_test) = data[1]
    X1_train, X2_train = split(X1_train, X2_train)
    print("Domain 1: {}".format(X1_train.shape))
    print("Domain 2: {}".format(X2_train.shape))

    X1_train = X1_train / 255
    X2_train = X2_train / 255
    X1_test = X1_test / 255
    X2_test = X2_test / 255

    # avoid one-to-one correspondences
    assert(X1_train.shape[0] == X2_train.shape[0] and
            X1_train.shape[0] % 2 == 0)
    I = np.random.permutation(X1_train.shape[0])
    I1 = I[:X1_train.shape[0]//2]
    I2 = I[X1_train.shape[0]//2:]
    X1_train = X1_train[I1,...]
    X2_train = X2_train[I2,...]

    # possibly restrict training size during debugging
    max_samples = None
    if max_samples is not None:
        X1_train = X1_train[:max_samples,...]
        X2_train = X2_train[:max_samples,...]

    print("After removing one-to-one correspondences.")
    print("Domain 1: {}".format(X1_train.shape))
    print("Domain 2: {}".format(X2_train.shape))

    model = ImageToImageTranslator(X1_train.shape[1:4])
    model.fit(X1_train, X2_train, X1_test, X2_test)
