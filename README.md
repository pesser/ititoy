# Image-to-Image Translation

Code to reproduce [Unsupervised Image-to-Image Translation Networks,
Ming-Yu Liu, Thomas Breuel, Jan
Kautz](https://arxiv.org/pdf/1703.00848.pdf) with Keras. Implements
Image-to-Image translation for an artificial dataset generated from MNIST as
described in the paper. Architecture follows mostly the paper but uses
upsampling followed by convolution instead of transposed convolution.

## Requirements
Keras with Tensorflow backend, Pillow and numpy should suffice to run the
model. For (optional) generation of the artifical dataset, OpenCV and
matplotlib are required additionally.

## Useage
To get the dataset either produce it with

    python main.py

or download it

    wget --content-disposition https://www.dropbox.com/s/d140hvnev9f7baj/mnist_domains.tar.gz?dl=1

and extract it. Then run

    python model.py <path to extracted data>

## Results
The code trains the model such that the training data does __not__ contain
one-to-one correspondences (i.e. the same MNIST image transformed to both
artifical domains) and no supervised signal for the similiarity of images.
It has only independent samples from the marginals of the joint image
distributions. The code produces the resulting reconstructions and translations
evaluated on the test data not seen during training in either domain.

The first and second domain look like this:

![transform 1](http://i.imgur.com/V8nRu28.png) ![transform 2](http://i.imgur.com/yv2hSXU.png)

In the next image, the first row shows the input from the test dataset in
the first domain, second row the reconstruction and third row the
translation to the second domain (all evolving over the first thirty
epochs):

![from1](http://i.imgur.com/J6iSvIj.gif)

The next image shows similiar results but this time the first row shows the
input from the test dataset in the second domain, the second row the
translation to the first domain and third row the reconstruction (again
evolving over the first thirty epochs):

![from2](http://i.imgur.com/q9mTFmi.gif)
