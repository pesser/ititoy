from keras.datasets import mnist
import matplotlib.pyplot as plt
import numpy as np
import cv2
import math


def tile(X):
    sqrt = math.sqrt(X.shape[0])
    rows = math.floor(sqrt)
    cols = math.ceil(sqrt)
    tiling = np.ones((rows * X.shape[1], cols * X.shape[2], X.shape[3]),
            dtype = X.dtype) * 255
    for i in range(rows):
        for j in range(cols):
            idx = i * cols + j
            if idx < X.shape[0]:
                img = X[idx,...]
                tiling[i*X.shape[1]:(i+1)*X.shape[1],
                        j*X.shape[2]:(j+1)*X.shape[2],
                        :] = img
    return tiling


def transform1(X, channel):
    assert(channel in [0,1,2])
    Y = np.zeros(X.shape + (3,), dtype = X.dtype)
    Y[:,:,channel] = X
    return Y

def rtransform1(X):
    return transform1(X, np.random.randint(3))


def transform2(X, channel):
    assert(channel in [0,1,2])
    canny_low_threshold = 50
    canny_high_threshold = 200
    edges = cv2.Canny(X, canny_low_threshold, canny_high_threshold,
            L2gradient = True)
    Y = np.zeros(X.shape + (3,), dtype = X.dtype)
    invchannels = [i for i in range(3) if i != channel]
    for i in range(3):
        if i != channel:
            Y[:,:,i] = edges
    return Y

def rtransform2(X):
    return transform2(X, np.random.randint(3))


def batch(X, trafo):
    return np.stack(map(trafo, X))


def visualize():
    (X_train, y_train), (X_test, y_test) = mnist.load_data()
    print(X_train.shape)
    print(y_train.shape)
    print(X_test.shape)
    print(y_test.shape)

    X_batch = X_train[:100,...]
    X1 = batch(X_batch, rtransform1)
    X2 = batch(X_batch, rtransform2)
    tX0 = np.squeeze(tile(np.expand_dims(X_batch, 3)))
    tX1 = tile(X1)
    tX2 = tile(X2)
    plt.imshow(tX0, cmap = "gray")
    plt.axis("off")
    plt.savefig("batch0.svg")
    plt.imshow(tX1)
    plt.axis("off")
    plt.savefig("batch1.svg")
    plt.imshow(tX2)
    plt.axis("off")
    plt.savefig("batch2.svg")


if __name__ == "__main__":
    import os, sys, pickle
    if len(sys.argv) != 2:
        print("Useage: {} <output path>".format(sys.argv[0]))
        exit(1)
    path = sys.argv[1]

    (X_train, y_train), (X_test, y_test) = mnist.load_data()
    X_train_1 = batch(X_train, rtransform1)
    X_test_1 = batch(X_test, rtransform1)
    X_train_2 = batch(X_train, rtransform2)
    X_test_2 = batch(X_test, rtransform2)

    domain_1_data = ((X_train_1, y_train), (X_test_1, y_test))
    domain_1_file = os.path.join(path, "mnist_domain_1.p")
    with open(domain_1_file, "wb") as f:
        pickle.dump(domain_1_data, f)
    domain_2_data = ((X_train_2, y_train), (X_test_2, y_test))
    domain_2_file = os.path.join(path, "mnist_domain_2.p")
    with open(domain_2_file, "wb") as f:
        pickle.dump(domain_2_data, f)

    mnist_data = ((X_train, y_train), (X_test, y_test))
    mnist_file = os.path.join(path, "mnist_original.p")
    with open(mnist_file, "wb") as f:
        pickle.dump(mnist_data, f)
